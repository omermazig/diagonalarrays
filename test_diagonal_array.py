from main import create_diagonals_array


def test_size_1():
    assert create_diagonals_array(1) == [[1]]


def test_size_2():
    assert create_diagonals_array(2) == [[1, 1],
                                         [1, 1]]


def test_size_3():
    assert create_diagonals_array(3) == [[1, 0, 1],
                                         [0, 1, 0],
                                         [1, 0, 1]]


def test_size_5():
    assert create_diagonals_array(5) == [[1, 0, 0, 0, 1],
                                         [0, 1, 0, 1, 0],
                                         [0, 0, 1, 0, 0],
                                         [0, 1, 0, 1, 0],
                                         [1, 0, 0, 0, 1]]


def test_size_8():
    assert create_diagonals_array(8) == [[1, 0, 0, 0, 0, 0, 0, 1],
                                         [0, 1, 0, 0, 0, 0, 1, 0],
                                         [0, 0, 1, 0, 0, 1, 0, 0],
                                         [0, 0, 0, 1, 1, 0, 0, 0],
                                         [0, 0, 0, 1, 1, 0, 0, 0],
                                         [0, 0, 1, 0, 0, 1, 0, 0],
                                         [0, 1, 0, 0, 0, 0, 1, 0],
                                         [1, 0, 0, 0, 0, 0, 0, 1]]
