def print_array(array):
    for i in array:
        print(i)
    print('\r\n')


def create_diagonals_array(size: int):
    array = [[[0] * size] * size]
    print_array(array)
    row = 0
    for column in range(size):
        array[column][row] = 1
        array[size - column][row] = 1
        row += 1
    print_array(array)
    return array


def main():
    size = int(input(f'Enter size for array: \n'))
    diagonals_array = create_diagonals_array(size)
    print_array(diagonals_array)


if __name__ == '__main__':
    main()
